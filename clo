#!/usr/bin/env bash

# Put it to /usr/bin/clo (working $PATH for cron jobs)
# CLO API docs: https://clo.ru/api

if ! [ -x "$(command -v curl)" ]; then
  echo 'Error: curl is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v jq)" ]; then
  echo 'Error: jq is not installed.' >&2
  exit 1
fi

CURL_OPTS="-k"

PROJECT_ID=''
SERVER_ID=''
SERVER_NAME=''

# Read PROJECT_ID, SERVER_ID from config or from API
function read_envs {
  if [[ $1 ]]; then 
    SERVER_NAME=$1
  fi
  test -f .clo && source .clo
  test -f ~/.clo && source ~/.clo
  if [ -z ${TOKEN} ]
    then
      echo "TOKEN is not set, exiting..."
      exit 1
  fi
  if [[ ${SERVER_ID} ]]; then 
    echo "Working with server ${SERVER_NAME}: ${SERVER_ID}"
  else 
    # echo 'Server name will be taken from $2'
    if [[ -z ${SERVER_NAME} ]]
      then
        echo "No server_name, exiting"
        $0 *
        exit 1
    else
      echo "SERVER_NAME: ${SERVER_NAME}"
      get_server_id_by_name "${SERVER_NAME}"
    fi
  fi
}

function is_ip_reserved {
    # do not insert one more echo
    URL="https://api.clo.ru/v1/floatingips/$1/detail"
    IP_DATA=$(curl ${CURL_OPTS} -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}")
    RESULT=$(echo ${IP_DATA} | jq -r '.result.floating_ip_address')
    echo $RESULT
}

function start_server {
  echo "Starting server ${SERVER_NAME} id ${SERVER_ID} ..."
  URL="https://api.clo.ru/v1/servers/${SERVER_ID}/start"
  RESULT=$(curl ${CURL_OPTS} -s -X POST -H "Authorization: Bearer ${TOKEN}" "${URL}")
  # echo ${RESULT}
  echo "Server ${SERVER_NAME} with id ${SERVER_ID} started"
}

function stop_server {
  echo "Stopping server ${SERVER_NAME} id ${SERVER_ID} ..."
  URL="https://api.clo.ru/v1/servers/${SERVER_ID}/stop"
  RESULT=$(curl ${CURL_OPTS} -s -X POST -H "Authorization: Bearer ${TOKEN}" "${URL}")
  # echo ${RESULT}
  echo "Server ${SERVER_NAME} with id ${SERVER_ID} stoped"
}

function get_project_id_from_token {
  URL="https://api.clo.ru/v1/projects"
  PROJECT_ID=$(curl ${CURL_OPTS} -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}" | jq -r '.results[0].id' )
}

function get_server_id_by_name {
  get_project_id_from_token
  URL="https://api.clo.ru/v1/projects/${PROJECT_ID}/servers"
  JQ_EXP=$(printf '.results[] | select(.name | contains("%s")) | .id' "$1")
  SERVER_ID=$(curl ${CURL_OPTS} -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}" | jq -r "${JQ_EXP}")
  
  if [ -z ${SERVER_ID} ]
    then
      echo "SERVER_ID not found, exiting. Maybe you provide wrong SERVER_NAME ${SERVER_NAME}"
      exit 1
  fi
  
  echo $SERVER_ID
}

case "$1" in
   up)
      read_envs "$2"
      start_server
      
      # Получение информации о сервере и из него внешний IP
      # URL="https://api.clo.ru/v1/servers/${SERVER_ID}/detail"
      # RESULT=$(curl ${CURL_OPTS} -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}")      
      # EXTERNAL_IP=$(echo ${RESULT} | jq -r '.result.addresses[] | select( .external == true ) | .name')
      # echo ${EXTERNAL_IP}
      
      URL="https://api.clo.ru/v1/projects/${PROJECT_ID}/floatingips"
      NEW_IP_DATA=$(curl ${CURL_OPTS} -s -X POST -H "Authorization: Bearer ${TOKEN}" "${URL}")
      # echo "${NEW_IP_DATA}"
      NEW_IP_UID=$(echo ${NEW_IP_DATA} | jq -r '.result.id')
      echo "Reserved new IP uid: ${NEW_IP_UID}"
          
      X=$(is_ip_reserved "${NEW_IP_UID}")
      echo "IP reserve status: ${X}"
      while [ "${X}" == 'null' ]
      do
       echo "Waiting 10s..."
       sleep 10
       X=$(is_ip_reserved "${NEW_IP_UID}")
      done
      
      # TO-DO: process error
      # {"code": 400, "title": "Bad request", "error": {"message": "You can't attach more than one floating ip to server."}}
      # если у сервера уже есть подключенный динамический IP то он выдаст сообщение выше
      DATA_JSON=$(printf '{"server_id": "%s"}' "${SERVER_ID}")
      URL="https://api.clo.ru/v1/floatingips/${NEW_IP_UID}/attach"
      RESULT=$(curl ${CURL_OPTS} -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" --data-raw "${DATA_JSON}" "${URL}")      
      # echo $RESULT
      IP_ADDR=$(echo ${RESULT} | jq -r '.result.floating_ip_address')
      echo "IP address ${IP_ADDR} attached to ${SERVER_NAME} ${SERVER_ID}"
      ;;
  down)
      read_envs "$2"
      stop_server
      
      # URL="https://api.clo.ru/v1/servers/${SERVER_ID}/detail"
      # RESULT=$(curl ${CURL_OPTS} -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}")      
      # echo ${RESULT}
      
      URL="https://api.clo.ru/v1/projects/${PROJECT_ID}/floatingips"
      RESULT=$(curl ${CURL_OPTS} -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}")      
      JQ_EXP=$(printf '.results[] | select( .attached_to_server.id == "%s" )' "${SERVER_ID}")
      IP_UID=$(echo ${RESULT} | jq -r "${JQ_EXP}" | jq -r ".id")
      IP_ADDR=$(echo ${RESULT} | jq -r "${JQ_EXP}" | jq -r ".floating_ip_address")
      # Delete floating IP
      URL="https://api.clo.ru/v1/floatingips/${IP_UID}"
      curl ${CURL_OPTS} -s -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer ${TOKEN}" "${URL}"
      # TO-DO: check curl status
      echo "Floating IP ${IP_ADDR} deleted"
      ;;   
  start)
    read_envs "$2"
    start_server
    ;;
  stop)
    read_envs "$2"
    stop_server
    ;;
  *)
    echo "Usage: clo {up|down|start|stop} <server_name>"
    echo " up.    Start server, create new floating IP and attach them"
    echo " down.  Stop server and remove its floating IP"
    echo " start. Just start specified server"
    echo " stop.  Just stop specified server"
    exit 1
    ;;
esac
